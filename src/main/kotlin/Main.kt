fun main(){
    val arr = intArrayOf(1,2,3,4);
    arr
        .rotateLeft(13)
        .forEachIndexed { i,v -> println(v) }
}

fun IntArray.rotateLeft(times: Int): IntArray {
    val rotate = if (times > size) times % size else times
    if(rotate == 0) return this
    var auxiliar = this.copyOf()
    forEachIndexed{ i, v ->
        if((i+rotate) < size)
            auxiliar[i] = this[i + rotate]
    }
    var i = 0
    var startIndex = size - rotate
    do{
        auxiliar[startIndex+i] = this[i]
        i++
    }while (i < rotate)
    return auxiliar
}


//println(reverseChars("reverse words in a string"))
fun reverseChars(s: String) = s.reversed()

//println(reverseWordsInPhrase("reverse words in a string"))
fun reverseWordsInPhrase(phrase: String) =
    phrase
        .split(" ")
        .reversed()
        .joinToString(" ")

fun mapOf(){
    val list = (1..5).toList()
    val map = list.map {
            index -> mapOf(0 to index,
        index % 3 to "Divided by 3")[0]
    }
    println(map)
}

fun downTo(){
    for(i in 8 downTo 0 step 2){
        print("$i ")
    }
}

fun toList(){
    val list = (1..5).toList()
    for(i in list){
        print("$i ")
    }
}

fun outer(){
    outer@
    for(i in 1..5) {
        for(j in  1..2){
            if(i == 3) break@outer
            print("($i, $j), ")
        }
    }
}